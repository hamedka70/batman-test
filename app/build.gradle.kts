@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.kapt)
    alias(libs.plugins.dagger.hilt.android)

}

android {
    namespace = "ir.karami.batman"
    applicationVariants.all {
        val variant = this
        variant.outputs
            .map { it as com.android.build.gradle.internal.api.BaseVariantOutputImpl }
            .forEach { output ->
                output.outputFileName = output.name + "-${variant.versionName}.apk"
            }
    }
    defaultConfig {
        versionName = libs.versions.app.version.name.get()
        configurations.all {
            resolutionStrategy { force(libs.androidx.core) }
        }


    }

    buildTypes {
        buildFeatures.buildConfig = true
        listOf("release", "debug").forEach {
            try {
                getByName(it)
            } catch (e: Exception) {
                create(it)
            }.apply {
                if (it == "release") {
                    isMinifyEnabled = true
                    isShrinkResources = true
                } else {
                    isMinifyEnabled = false
                    isDebuggable = true
                    enableAndroidTestCoverage = false
                    versionNameSuffix = "-$it"
                    applicationIdSuffix = ".$it"
                }
                proguardFiles(
                    getDefaultProguardFile("proguard-android-optimize.txt"),
                    "proguard-rules.pro"
                )
            }
        }
    }


    kapt {
        correctErrorTypes = true
    }

    buildFeatures {
        dataBinding = true
        viewBinding = true
        compose = true
    }
    lint {
        abortOnError = false
        checkReleaseBuilds = false
        baseline = file("lint-baseline.xml")
        checkReleaseBuilds = false
    }
}

kapt{
    correctErrorTypes = true
}


dependencies {
    implementation(libs.androidx.core)
    implementation(libs.androidx.activity.compose)
    implementation(platform(libs.compose.bom))
    implementation(libs.compose.ui)
    implementation(libs.compose.ui.graphics)
    implementation("androidx.hilt:hilt-navigation-compose:1.0.0")

    implementation(libs.compose.material3)
    implementation(libs.androidx.lifecycle.viewmodel.compose)
    debugImplementation(libs.compose.ui.tooling.preview)
    implementation(libs.compose.material3)
    implementation(libs.androidx.legecy)
    implementation(libs.androidx.annotation)
    implementation(libs.kotlin.coroutines.core)
    implementation(libs.kotlin.coroutines.android)
    implementation(libs.dagger.hilt.android)
    kapt(libs.dagger.hilt.android.compiler)
    debugImplementation(libs.androidx.ui.tooling)
    implementation(projects.di)
    implementation(libs.mvikotlin.core)
    implementation(libs.mvikotlin.coroutine)
    implementation(libs.mvikotlin.logging)
    implementation(libs.mvikotlin.main)
    implementation(libs.mvikotlin.timetravel)
    implementation(libs.coil.coil)
    implementation(libs.compose.navigation)
}
