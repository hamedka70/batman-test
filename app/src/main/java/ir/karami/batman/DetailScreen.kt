package ir.karami.batman

import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import ir.karami.batman.store.DetailStore

@Composable
fun DetailScreen(
    onBackClick: () -> Unit,
    state: DetailStore.State
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        Spacer(modifier = Modifier.size(8.dp))
        FloatingActionButton(
            onClick = {
                onBackClick()
            }) {
            Image(
                painter = painterResource(id = R.drawable.arrow_back),
                contentDescription = "arrow back"
            )
        }

        Spacer(modifier = Modifier.size(8.dp))
        LazyColumn {

            item {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(
                        text = state.movieDetail?.title ?: "",
                        style = MaterialTheme.typography.titleLarge
                    )
                    Text(
                        text = state.movieDetail?.year ?: "",
                        style = MaterialTheme.typography.titleMedium
                    )
                }
            }

            item {
                Spacer(modifier = Modifier.size(16.dp))
                AsyncImage(
                    model = state.movieDetail?.poster ?: "",
                    contentDescription = "",
                    modifier = Modifier
                        .fillMaxWidth()
                        .size(300.dp)
                )
                Spacer(modifier = Modifier.size(16.dp))
            }
            item {
                Spacer(modifier = Modifier.size(8.dp))

            }
            items(
                listOf(
                    "Rated :" to state.movieDetail?.rated,
                    "Released :" to state.movieDetail?.released,
                    "Runtime :" to state.movieDetail?.runtime,
                    "Director :" to state.movieDetail?.director,
                    "Writer :" to state.movieDetail?.writer,
                    "Actors :" to state.movieDetail?.actors,
                    "Plot :" to state.movieDetail?.plot,
                    "Language :" to state.movieDetail?.language,
                    "Country :" to state.movieDetail?.country,
                    "Awards :" to state.movieDetail?.awards,
                    "Metascore :" to state.movieDetail?.metascore,
                    "imdbRating :" to state.movieDetail?.imdbRating,
                    "imdbVotes :" to state.movieDetail?.imdbVotes,
                    "imdbID :" to state.movieDetail?.imdbID,
                    "Type :" to state.movieDetail?.type,
                    "DVD :" to state.movieDetail?.dvd,
                    "BoxOffice :" to state.movieDetail?.boxOffice,
                    "Production :" to state.movieDetail?.production,
                    "Website :" to state.movieDetail?.website
                )
            ) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Text(
                        text = it.first,
                        modifier = Modifier
                            .padding(4.dp),
                        style = MaterialTheme.typography.titleSmall
                    )
                    Text(
                        text = it.second ?: "                       ",
                        modifier = Modifier
                            .padding(vertical = 4.dp)
                            .background(shimmerBrush(showShimmer = it.second == null)),
                        style = MaterialTheme.typography.labelMedium
                    )
                }

            }

            item {
                Column {
                    Text(text = "Rating" , style = MaterialTheme.typography.titleSmall)
                    Spacer(modifier = Modifier.size(8.dp))
                    LazyRow{
                        state.movieDetail?.rating?.let {
                            items(it){
                                Column(modifier = Modifier.width(width = 140.dp).padding(8.dp)) {
                                    Text(text = "Source: ${it.source}" ,style = MaterialTheme.typography.titleSmall)
                                    Text(text = "Value: ${it.value}" ,style = MaterialTheme.typography.titleSmall)
                                }
                            }
                        }

                    }
                }
                Spacer(modifier = Modifier.size(16.dp))

            }
        }

    }
}

@Composable
fun shimmerBrush(showShimmer: Boolean = true, targetValue: Float = 1000f): Brush {
    return if (showShimmer) {
        val shimmerColors = listOf(
            Color.LightGray.copy(alpha = 0.6f),
            Color.LightGray.copy(alpha = 0.2f),
            Color.LightGray.copy(alpha = 0.6f),
        )

        val transition = rememberInfiniteTransition(label = "")
        val translateAnimation = transition.animateFloat(
            initialValue = 0f,
            targetValue = targetValue,
            animationSpec = infiniteRepeatable(
                animation = tween(800), repeatMode = RepeatMode.Reverse
            ), label = ""
        )
        Brush.linearGradient(
            colors = shimmerColors,
            start = Offset.Zero,
            end = Offset(x = translateAnimation.value, y = translateAnimation.value)
        )
    } else {
        Brush.linearGradient(
            colors = listOf(Color.Transparent, Color.Transparent),
            start = Offset.Zero,
            end = Offset.Zero
        )
    }
}

