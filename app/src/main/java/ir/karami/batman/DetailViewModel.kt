package ir.karami.batman

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.karami.batman.store.DetailStoreProvider
import javax.inject.Inject


@HiltViewModel
class DetailViewModel @Inject constructor(
    private val detailStoreProvider: DetailStoreProvider,
): ViewModel() {

    val store by lazy {
        detailStoreProvider()
    }

    override fun onCleared() {
        store.dispose()
        super.onCleared()
    }
}