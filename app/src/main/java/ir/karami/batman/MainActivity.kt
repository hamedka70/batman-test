package ir.karami.batman

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import dagger.hilt.android.AndroidEntryPoint
import ir.karami.batman.compose.BatmanApp
import ir.karami.batman.ui.theme.BatmanTheme
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update


@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BatmanTheme(darkTheme = ThemeHelper.isLightThemeState.collectAsState()) {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    BatmanApp()
                }
            }
        }
    }
}


object ThemeHelper {
    private val _isLightThemeState = MutableStateFlow(true)
    val isLightThemeState: StateFlow<Boolean> = _isLightThemeState
    fun toggle() {
        _isLightThemeState.update {
            it.not()
        }
    }

    fun getIsDarkMode(): Boolean = _isLightThemeState.value
}

