package ir.karami.batman

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.karami.batman.store.MainStore
import ir.karami.batman.store.MainStoreProvider
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainStoreProvider: MainStoreProvider
) : ViewModel() {
    val store by lazy {
        mainStoreProvider()
    }

    fun refreshPage(){
        store.accept(
            MainStore.Intent.RefreshList
        )
    }

    override fun onCleared() {
        store.dispose()
        super.onCleared()
    }



}