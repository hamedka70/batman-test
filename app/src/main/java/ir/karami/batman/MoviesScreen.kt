package ir.karami.batman

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.arkivanov.mvikotlin.extensions.coroutines.stateFlow
import ir.karami.batman.store.MainStore
import ir.karami.domain.movie.Movie
import timber.log.Timber


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MoviesScreen(
    viewModel: MainViewModel = hiltViewModel(),
    onMovieClick: (imdbID: String) -> Unit = {}
) {
    val state by viewModel.store.stateFlow.collectAsState()
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primaryContainer
                ),
                title = {
                    Text(text = "Batman")
                },
                navigationIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.batman),
                        contentDescription = "darak mode toggle",
                        modifier = Modifier
                            .size(44.dp)
                            .padding(horizontal = 8.dp)
                    )
                },
                actions = {
                    IconButton(onClick = {
                        ThemeHelper.toggle()
                    }) {
                        Icon(
                            painter = painterResource(id = R.drawable.dark_mode),
                            contentDescription = "darak mode",
                            tint = if (ThemeHelper.getIsDarkMode()) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.error
                        )
                    }
                }
            )

        },
        content = {
            when (val mState = state.listState) {
                is MainStore.State.ListState.Data -> MovieList(
                    mState = mState.movies,
                    paddingValues = it,
                    onMovieClick = onMovieClick
                )
                MainStore.State.ListState.Empty ->  EmptyStateScreen(viewModel = viewModel)
            }
        }
    )
}


@Composable
fun EmptyStateScreen(viewModel: MainViewModel) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
        ) {

        Icon(
            painter = painterResource(id = R.drawable.wifi_off),
            contentDescription = "connection error",
            tint = MaterialTheme.colorScheme.error,
            modifier = Modifier.size(120.dp))

        Spacer(modifier = Modifier.size(8.dp))

        Text(
            text = "Check youre internet!",
            style = MaterialTheme.typography.titleLarge
        )

        Spacer(modifier = Modifier.size(24.dp))

        Button(onClick ={
            viewModel.refreshPage()
        } ) {
            Text(text = "Refresh...")
        }
    }
}

@Composable
fun MovieList(
    mState: List<Movie>,
    paddingValues: PaddingValues,
    onMovieClick: (imdbID: String) -> Unit
) {
    LazyColumn(modifier = Modifier
        .fillMaxWidth()
        .padding(paddingValues), content = {
        items(mState) {
            MovieItem(movie = it, onMovieClick = onMovieClick)
        }
    })
}

@Composable
fun MovieItem(movie: Movie, onMovieClick: (imdbID: String) -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .height(100.dp)
            .clickable {
                onMovieClick(movie.imdbID)
            }
    ) {
        AsyncImage(
            model = movie.poster,
            contentDescription = movie.title,
            modifier = Modifier
                .width(80.dp)
                .clip(RoundedCornerShape(16.dp)),
            placeholder = painterResource(id = R.drawable.baseline_4k_24),
            onError = {
                Timber.tag("error").e(it.result.throwable.message)
            }
        )
        Spacer(modifier = Modifier.size(8.dp))
        Column(
            modifier = Modifier.fillMaxHeight(),
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(
                    text = "Title:  ",
                    style = MaterialTheme.typography.titleMedium
                )
                Text(
                    text = movie.title,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    style = MaterialTheme.typography.labelMedium
                )
            }
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(
                    text = "Years:  ",
                    style = MaterialTheme.typography.titleMedium
                )
                Text(
                    text = movie.year,
                    style = MaterialTheme.typography.labelMedium
                )
            }

        }
        Spacer(modifier = Modifier.size(4.dp))

    }
}