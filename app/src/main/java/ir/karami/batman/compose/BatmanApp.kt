/*
 * Copyright 2023 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ir.karami.batman.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.arkivanov.mvikotlin.extensions.coroutines.stateFlow
import ir.karami.batman.DetailScreen
import ir.karami.batman.DetailViewModel
import ir.karami.batman.MoviesScreen
import ir.karami.batman.store.DetailStore

@Composable
fun BatmanApp(
) {
    val navController = rememberNavController()
    BatmanNavHost(
        navController = navController
    )
}

@Composable
fun BatmanNavHost(
    navController: NavHostController
) {
    NavHost(navController = navController, startDestination = "home") {
        composable("home") {
            MoviesScreen(
                onMovieClick = {
                    navController.navigate("detail/${it}")
                },
            )
        }
        composable(
            "detail/{imdbID}",
            arguments = listOf(navArgument("imdbID") {
                type = NavType.StringType
            })
        ) {

            val viewmodel = hiltViewModel<DetailViewModel>()
            it.arguments?.getString("imdbID")?.let {
                viewmodel.store.accept(DetailStore.Intent.RefreshDetail(it))
                viewmodel.store.accept(DetailStore.Intent.GetDetail(it))
            }
            DetailScreen(
                onBackClick = { navController.navigateUp() },
                state = viewmodel.store.stateFlow.collectAsState().value
            )
        }
    }
}
