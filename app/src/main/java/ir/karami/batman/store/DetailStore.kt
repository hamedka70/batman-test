package ir.karami.batman.store

import com.arkivanov.mvikotlin.core.store.Reducer
import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.arkivanov.mvikotlin.core.utils.ExperimentalMviKotlinApi
import com.arkivanov.mvikotlin.extensions.coroutines.CoroutineExecutor
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineBootstrapper
import ir.karami.domain.movie.Movie
import ir.karami.domain.movie.MovieRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

interface DetailStore {
    data class State(
        val movieDetail: Movie?
    )

    sealed interface Intent {
        data class GetDetail(val id: String) : Intent
        data class RefreshDetail(val id: String): Intent
    }


}

class DetailStoreProvider @Inject constructor(
    private val storeFactory: StoreFactory,
    private val movieRepository: MovieRepository
) : CoroutineExecutor<DetailStore.Intent, DetailStoreProvider.Action, DetailStore.State, DetailStoreProvider.Msg, Unit>(),
    Reducer<DetailStore.State, DetailStoreProvider.Msg> {

    sealed interface Action

    private var job: Job? =null

    override fun executeIntent(intent: DetailStore.Intent, getState: () -> DetailStore.State) {
        when(intent){
            is DetailStore.Intent.GetDetail -> {
                job?.cancel()
                job = scope.launch {
                    movieRepository.movieDetail(intent.id).stateIn(this).collectLatest {
                        dispatch(Msg.OnGetDetail(it))
                    }
                }
            }

            is DetailStore.Intent.RefreshDetail -> scope.launch {
                movieRepository.refreshDetail(intent.id)
            }
        }
    }
    @OptIn(ExperimentalMviKotlinApi::class)
    operator fun invoke(): Store<DetailStore.Intent , DetailStore.State , Unit> =
            storeFactory.create(
                name = "DetailStoreProvider",
                initialState = DetailStore.State(null),
                executorFactory = { this },
                reducer = this
            )

    sealed interface Msg {
        data class OnGetDetail(val movie: Movie) : Msg

    }

    override fun DetailStore.State.reduce(msg: Msg): DetailStore.State {
       return when (msg) {
           is Msg.OnGetDetail -> copy(movieDetail = msg.movie)
       }
    }


}