package ir.karami.batman.store

import com.arkivanov.mvikotlin.core.store.Reducer
import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.arkivanov.mvikotlin.core.utils.ExperimentalMviKotlinApi
import com.arkivanov.mvikotlin.extensions.coroutines.CoroutineExecutor
import com.arkivanov.mvikotlin.extensions.coroutines.coroutineBootstrapper
import ir.karami.domain.movie.Movie
import ir.karami.domain.movie.MovieRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

interface MainStore {
    data class State(
        val listState: ListState = ListState.Empty
    ) {
        sealed interface ListState {
            data object Empty: ListState
            data class Data(val movies: List<Movie>): ListState
        }
    }
    sealed interface Intent {
        data object RefreshList: Intent


    }
}

class MainStoreProvider @Inject constructor(
    private val storeFactory: StoreFactory,
    private val movieRepository: MovieRepository
) : CoroutineExecutor<MainStore.Intent, MainStoreProvider.Action, MainStore.State, MainStoreProvider.Msg, Nothing>(),
    Reducer<MainStore.State, MainStoreProvider.Msg> {


    override fun executeAction(action: Action, getState: () -> MainStore.State) {
        when (action) {
            Action.FetchMovies -> {
                scope.launch(Dispatchers.IO) {
                    movieRepository.refreshMoviesList()
                }
            }

            Action.GetMovies -> {
                scope.launch {
                    movieRepository.movies().flowOn(Dispatchers.Default).stateIn(this).collectLatest {
                        dispatch(Msg.OnGetNewList(it))
                    }
                }
            }
        }

    }


    override fun executeIntent(intent: MainStore.Intent, getState: () -> MainStore.State) {
        when (intent) {
            MainStore.Intent.RefreshList -> executeAction(Action.FetchMovies,getState)
        }
    }

    operator fun invoke(): Store<MainStore.Intent, MainStore.State, Nothing> = storeFactory.create(
        name = "RootStoreProvider",
        initialState = MainStore.State(),
        executorFactory = { this },
        bootstrapper = bootstrapper,
        reducer = this
    )

    @OptIn(ExperimentalMviKotlinApi::class)
    private val bootstrapper by lazy {
        coroutineBootstrapper {
            this.dispatch(Action.FetchMovies)
            this.dispatch(Action.GetMovies)

        }
    }

    sealed interface Action {
        data object FetchMovies: Action
        data object GetMovies: Action
    }

    sealed interface Msg {
        data class OnGetNewList(val list: List<Movie>): Msg

    }

    override fun MainStore.State.reduce(msg: Msg): MainStore.State {
        return when (msg) {
            is Msg.OnGetNewList -> copy(
                listState = if (msg.list.isNotEmpty()) {
                    MainStore.State.ListState.Data(msg.list)
                } else {
                    listState
                }

            )

        }
    }

}
