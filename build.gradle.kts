@Suppress("DSL_SCOPE_VIOLATION") plugins {
    alias(libs.plugins.android.application) apply false
    alias(libs.plugins.android.library) apply false
    alias(libs.plugins.android.lint) apply false
    alias(libs.plugins.android.test) apply false
    alias(libs.plugins.kotlin.android) apply false
    alias(libs.plugins.kotlin.kapt) apply false
    alias(libs.plugins.kotlin.serialization) apply false
    alias(libs.plugins.ksp) apply false
    alias(libs.plugins.dagger.hilt.android) apply false
    alias(libs.plugins.org.jetbrains.kotlin.jvm) apply false

}

buildscript {

    repositories {
        google()
        maven(url = "'https://mvnrepository.com/artifact/com.github.gundy/semver4j")
        mavenCentral()
    }
}
allprojects {
    repositories {
        google()
        maven(url = "https://jitpack.io")
        mavenCentral()
    }
    tasks {
        withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
            kotlinOptions {
                freeCompilerArgs += listOf("-Xskip-prerelease-check")
            }
        }
    }

    tasks.withType(org.jetbrains.kotlin.gradle.tasks.KotlinCompile::class).all {
        kotlinOptions.freeCompilerArgs = listOf("-Xcontext-receivers")
    }


}


subprojects {
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
        kotlinOptions {
            allWarningsAsErrors = false
            freeCompilerArgs += listOf(
                "-opt-in=kotlinx.coroutines.ExperimentalCoroutinesApi",
                "-opt-in=kotlinx.coroutines.FlowPreview",
                "-opt-in=kotlin.Experimental",
                "-opt-in=kotlinx.serialization.ExperimentalSerializationApi"
            )
            jvmTarget = JavaVersion.VERSION_17.toString()
        }
    }

    plugins.withId(rootProject.libs.plugins.dagger.hilt.android.get().pluginId) {
        extensions.getByType<dagger.hilt.android.plugin.HiltExtension>().enableAggregatingTask =
            true
    }

    plugins.withId(rootProject.libs.plugins.kotlin.kapt.get().pluginId) {
        extensions.getByType<org.jetbrains.kotlin.gradle.plugin.KaptExtension>().correctErrorTypes =
            true
    }
    plugins.withType<com.android.build.gradle.BasePlugin>().configureEach {
        extensions.configure<com.android.build.gradle.BaseExtension> {

            compileSdkVersion(libs.versions.compileSdk.get())

            defaultConfig {
                minSdk = libs.versions.minSdk.get().toInt()
                targetSdk = libs.versions.targetSdk.get().toInt()
            }
            compileOptions {
                sourceCompatibility = JavaVersion.VERSION_17
                targetCompatibility = JavaVersion.VERSION_17
            }
            composeOptions {
                kotlinCompilerExtensionVersion = libs.versions.compose.compiler.get()
            }
            buildFeatures.apply {
                dataBinding.enable = true
                viewBinding = true
            }
        }
        dependencies {
            add("implementation", libs.timber)
        }
    }
}
