package ir.karami.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import ir.karami.data.database.dao.Dao
import ir.karami.data.database.entity.Movies
import ir.karami.data.database.entity.Rating


@Database(entities = [Movies::class, Rating::class], version = 5, exportSchema = true)
abstract class MovieDatabase : RoomDatabase() {
    abstract fun movieDao(): Dao

}