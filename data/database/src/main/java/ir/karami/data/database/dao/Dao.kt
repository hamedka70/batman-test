package ir.karami.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ir.karami.data.database.entity.Movies
import ir.karami.data.database.entity.Rating
import kotlinx.coroutines.flow.Flow

@Dao
interface Dao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity: List<Movies>): List<Long>

    @Query("Select * from movies")
    fun getAllMovies(): Flow<List<Movies>>

    @Query("delete from movies where imdbID not in (:ids)")
    suspend fun deleteOthers(ids: List<String>)

    @Query("Select * from movies where imdbID == (:imdbID)")
    fun getMovie(imdbID: String): Flow<Movies>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRating(rating: List<Rating>): List<Long>

    @Query("Select * from rating where rating_imdbID == (:imdbID)")
    fun getRating(imdbID: String): List<Rating>

    @Query("Select * from movies left join rating on movies.imdbID=rating.rating_imdbID where imdbID=:imdbID")
    fun getMovieByRatings(imdbID: String): Flow<Map<Movies,List<Rating>>>

}