package ir.karami.data.database.di

import android.content.Context
import android.os.Debug
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ir.karami.data.database.MovieDatabase
import ir.karami.data.database.dao.Dao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ProviderDi {
    @Provides
    @Singleton
    fun provideMovieDao(database: MovieDatabase): Dao = database.movieDao()
    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context
    ): MovieDatabase {
        val builder = Room.databaseBuilder(context, MovieDatabase::class.java, "movie.db")
            .fallbackToDestructiveMigration()
        if (Debug.isDebuggerConnected()) {
            builder.allowMainThreadQueries()
        }
        return builder.build()
    }
}