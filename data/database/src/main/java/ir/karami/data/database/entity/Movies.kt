package ir.karami.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "movies",
)
data class Movies(
    @PrimaryKey
    @ColumnInfo(name = "imdbID")
    val imdbID: String,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "year")
    val year: String,
    @ColumnInfo(name = "poster")
    val poster: String,
    @ColumnInfo(name = "Rated")
    val rated: String? = null,
    @ColumnInfo(name = "Released")
    val released: String? = null,
    @ColumnInfo(name = "Runtime")
    val runtime: String? = null,
    @ColumnInfo(name = "Genre")
    val genre: String? = null,
    @ColumnInfo(name = "Director")
    val director: String? = null,
    @ColumnInfo(name = "Writer")
    val writer: String? = null,
    @ColumnInfo(name = "Actors")
    val actors: String? = null,
    @ColumnInfo(name = "Plot")
    val plot: String? = null,
    @ColumnInfo(name = "Country")
    val country: String? = null,
    @ColumnInfo(name = "Language")
    val language: String? = null,
    @ColumnInfo(name = "Awards")
    val awards: String? = null,
    @ColumnInfo(name = "Metascore")
    val metascore: String? =null,
    @ColumnInfo(name = "imdbRating")
    val imdbRating: String? = null,
    @ColumnInfo(name = "imdbVotes")
    val imdbVotes: String? = null,
    @ColumnInfo(name = "Type")
    val type: String? = null,
    @ColumnInfo(name = "DVD")
    val dvd: String? = null,
    @ColumnInfo(name = "BoxOffice")
    val boxOffice: String? = null,
    @ColumnInfo(name = "Production")
    val production: String? = null,
    @ColumnInfo(name = "website")
    val website: String? = null
)

@Entity(
    tableName = "rating",
    foreignKeys = arrayOf(
        ForeignKey(
            entity = Movies::class,
            parentColumns = arrayOf("imdbID"),
            childColumns = arrayOf("rating_imdbID"),
            onDelete = ForeignKey.CASCADE

        )
    )
)
data class Rating(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo("rate_id")
    val id: Long = 0,
    @ColumnInfo(name = "rating_imdbID") val imdbID: String,
    @ColumnInfo(name = "Source") val source: String,
    @ColumnInfo(name = "value") val value: String
)