package ir.karami.data.network.base

object EndPoints {

    const val getMoviesList = "https://www.omdbapi.com/?apikey=3e974fca&s=batman"
    const val getMovieDetail = "https://www.omdbapi.com/?apikey=3e974fca"
}