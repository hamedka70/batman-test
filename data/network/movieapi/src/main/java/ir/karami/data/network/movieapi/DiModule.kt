package ir.karami.data.network.movieapi

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
internal abstract class DiModule {
    @Binds
    abstract fun bindMovieApi(movieApiImpl: MovieApiImpl): MovieApi
}