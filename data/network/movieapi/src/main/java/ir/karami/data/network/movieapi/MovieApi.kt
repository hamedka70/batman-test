package ir.karami.data.network.movieapi

import ir.karami.data.network.movieapi.model.Movie
import ir.karami.data.network.movieapi.model.Movies

interface MovieApi {
    suspend fun movies(): Movies
    suspend fun movieDetail(imdbID: String): Movie
}