package ir.karami.data.network.movieapi

import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.statement.bodyAsText
import ir.karami.data.network.base.ApiClient
import ir.karami.data.network.base.EndPoints
import ir.karami.data.network.movieapi.model.Movie
import ir.karami.data.network.movieapi.model.Movies
import javax.inject.Inject

internal class MovieApiImpl @Inject constructor(
    private val apiClient: ApiClient
): MovieApi {
    override suspend fun movies(): Movies =
        apiClient.getKtorClient().get(EndPoints.getMoviesList).body()

    override suspend fun movieDetail(imdbID: String): Movie {

        val result = apiClient.getKtorClient().get(EndPoints.getMovieDetail) {
            parameter("i", imdbID)
        }
        result.bodyAsText()
        return result.body()
    }
}