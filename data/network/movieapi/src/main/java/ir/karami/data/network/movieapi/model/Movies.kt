package ir.karami.data.network.movieapi.model

import kotlinx.serialization.SerialName

@kotlinx.serialization.Serializable
data class Movies(
    @SerialName("Search") val movies: List<Movie>,
    @SerialName("totalResults") val totalResults: String,
    @SerialName("Response") val response: Boolean,

)

@kotlinx.serialization.Serializable
data class Movie(
    @SerialName("Title") val title: String,
    @SerialName("Year") val year: String,
    @SerialName("imdbID") val imdbID: String,
    @SerialName("Poster") val poster: String,
    @SerialName("Rated") val rated: String? =null,
    @SerialName("Released") val released: String? = null,
    @SerialName("Runtime") val runtime: String? = null,
    @SerialName("Genre") val genre: String? = null,
    @SerialName("Director") val director: String? = null,
    @SerialName("Writer") val writer: String? =null,
    @SerialName("Country") val country: String? =null,
    @SerialName("Actors") val actors: String? =null,
    @SerialName("Plot") val plot: String? =null,
    @SerialName("Language") val language: String? =null,
    @SerialName("Awards") val awards: String? =null,
    @SerialName("Ratings") val rating: List<Rating> = emptyList(),
    @SerialName("Metascore") val metascore: String? =null,
    @SerialName("imdbRating") val imdbRating: String? =null,
    @SerialName("imdbVotes") val imdbVotes: String? =null,
    @SerialName("Type") val type: String? =null,
    @SerialName("DVD") val dvd: String? =null,
    @SerialName("BoxOffice") val boxOffice: String? =null,
    @SerialName("Production") val production: String? =null,
    @SerialName("website") val website: String? =null,
)

@kotlinx.serialization.Serializable
data class Rating(
    @SerialName("Source") val source: String,
    @SerialName("Value") val value: String
)