@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.kapt)
    alias(libs.plugins.dagger.hilt.android)
    alias(libs.plugins.kotlin.serialization)
}

android {
    namespace = "ir.karami.di"
}

dependencies {
    api(libs.gson)
    implementation(libs.dagger.hilt.android)
    kapt(libs.dagger.hilt.android.compiler)
    api(libs.kotlinx.serialization.json)

    api(projects.domain)
    implementation(projects.data.network.movieapi)
    implementation(projects.data.network.base)
    implementation(projects.data.database)
    implementation(projects.repos.movies)
    implementation(projects.repos.base)
}