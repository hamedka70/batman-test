package ir.karami.domain.movie

data class Movie(
    val title: String,
    val year: String,
    val imdbID: String,
    val poster: String,
    val rated: String? = null,
    val released: String? = null,
    val runtime: String? = null,
    val genre: String? = null,
    val director: String? = null,
    val writer: String? = null,
    val country: String? = null,
    val actors: String? = null,
    val plot: String? = null,
    val language: String? = null,
    val awards: String? = null,
    val rating: List<Rating>? = null,
    val metascore: String? = null,
    val imdbRating: String? = null,
    val imdbVotes: String? = null,
    val type: String? = null,
    val dvd: String? = null,
    val boxOffice: String? = null,
    val production: String? = null,
    val website: String? = null
)

data class Rating(
    val source: String,
    val value: String
)
