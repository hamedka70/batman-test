package ir.karami.domain.movie

import kotlinx.coroutines.flow.Flow


interface MovieRepository {
    suspend fun movies(): Flow<List<Movie>>
    suspend fun refreshMoviesList()

    suspend fun movieDetail(imdbID: String): Flow<Movie>

    suspend fun refreshDetail(imdbID: String)
}