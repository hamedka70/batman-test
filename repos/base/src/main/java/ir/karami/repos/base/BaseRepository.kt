package ir.karami.repos.base

import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

abstract class BaseRepository {
    inline fun <reified T: Any> callWebService(
        crossinline body: suspend () -> T
    ) = flow {
        val result = body.invoke()
        emit(Result.success(result))
    }.catch {
        emit(Result.failure(it))
    }

    suspend inline fun <reified T : Any> suspendCallWebService(
        crossinline body: suspend () -> T
    ) = try {
        val result = body.invoke()
        Result.success(result)
    } catch (e: Throwable) {
        Result.failure(e)
    }

}