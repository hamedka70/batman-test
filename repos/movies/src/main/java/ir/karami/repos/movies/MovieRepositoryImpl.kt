package ir.karami.repos.movies

import ir.karami.data.database.MovieDatabase
import ir.karami.data.database.entity.Movies
import ir.karami.data.network.movieapi.MovieApi
import ir.karami.domain.movie.Movie
import ir.karami.domain.movie.MovieRepository
import ir.karami.domain.movie.Rating
import ir.karami.repos.base.BaseRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filterNot
import kotlinx.coroutines.flow.map
import javax.inject.Inject


class MovieRepositoryImpl @Inject constructor(
    private val movieApi: MovieApi,
    private val movieDatabase: MovieDatabase
) : MovieRepository, BaseRepository() {
    override suspend fun movies(): Flow<List<Movie>> = movieDatabase.movieDao().getAllMovies().map {
        it.map {
            Movie(
                title = it.title,
                year = it.year,
                imdbID = it.imdbID,
                poster = it.poster
            )
        }
    }

    override suspend fun refreshMoviesList() {
        val result = runCatching {
            movieApi.movies()
        }
        result.getOrNull()?.movies?.map {
            Movies(
                title = it.title,
                year = it.year,
                imdbID = it.imdbID,
                poster = it.poster
            )
        }?.let {
            movieDatabase.movieDao().deleteOthers(it.map { it.imdbID })
            movieDatabase.movieDao().insert(it)
        }
    }

    override suspend fun movieDetail(imdbID: String): Flow<Movie> =
        movieDatabase.movieDao().getMovieByRatings(imdbID).filterNot { it.isEmpty() }.map {
            it.map {
                Movie(
                    imdbID = it.key.imdbID,
                    title = it.key.title,
                    year = it.key.year,
                    poster = it.key.poster,
                    rated = it.key.rated,
                    released = it.key.released,
                    runtime = it.key.runtime,
                    genre = it.key.genre,
                    director = it.key.director,
                    writer = it.key.writer,
                    actors = it.key.actors,
                    plot = it.key.plot,
                    language = it.key.language,
                    awards = it.key.awards,
                    country = it.key.country,
                    metascore = it.key.metascore,
                    imdbRating = it.key.imdbRating,
                    imdbVotes = it.key.imdbVotes,
                    type = it.key.type,
                    dvd = it.key.dvd,
                    boxOffice = it.key.boxOffice,
                    production = it.key.production,
                    website = it.key.website,
                    rating = it.value.map {
                        Rating(
                            source = it.source,
                            value = it.value
                        )
                    }
                )
            }.first()
        }


    override suspend fun refreshDetail(imdbID: String) {
        val result = runCatching {
            movieApi.movieDetail(imdbID)
        }

        result.getOrNull()?.let {
            movieDatabase.movieDao().insert(
                listOf(
                    Movies(
                        imdbID = it.imdbID,
                        title = it.title,
                        year = it.year,
                        poster = it.poster,
                        rated = it.rated,
                        released = it.released,
                        runtime = it.runtime,
                        genre = it.genre,
                        director = it.director,
                        writer = it.writer,
                        actors = it.actors,
                        plot = it.plot,
                        language = it.language,
                        awards = it.awards,
                        country = it.country,
                        metascore = it.metascore,
                        imdbRating = it.imdbRating,
                        imdbVotes = it.imdbVotes,
                        type = it.type,
                        dvd = it.dvd,
                        boxOffice = it.boxOffice,
                        production = it.production,
                        website = it.website,

                    )
                )

            )

            movieDatabase.movieDao().insertRating(
                it.rating.map { rating ->
                    ir.karami.data.database.entity.Rating(
                        imdbID = it.imdbID,
                        source = rating.source,
                        value = rating.value
                    )
                }
            )
        }
    }

}

