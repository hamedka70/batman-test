package ir.karami.repos.movies.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.karami.domain.movie.MovieRepository
import ir.karami.repos.movies.MovieRepositoryImpl


@Module
@InstallIn(SingletonComponent::class)
internal abstract class DiModule {

    @Binds
    internal abstract fun bindMovieRepository(movieRepositoryImpl: MovieRepositoryImpl): MovieRepository
}
