package ir.karami.repos.movies.mapper

interface Mapper<T, R> {
  operator fun invoke(param: T, vararg args: String?): R
}
