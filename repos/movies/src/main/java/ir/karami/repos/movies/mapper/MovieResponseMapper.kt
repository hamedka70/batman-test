package ir.karami.repos.movies.mapper

import ir.karami.data.network.movieapi.model.Movie
import javax.inject.Inject

class MovieResponseMapper @Inject constructor(): Mapper<Movie, ir.karami.domain.movie.Movie> {
    override fun invoke(param: Movie, vararg args: String?)= ir.karami.domain.movie.Movie(
        title = param.title,
        year = param.year,
        imdbID = param.imdbID,
        poster = param.poster
    )
}