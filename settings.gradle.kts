rootProject.name = "Batman"

pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

include(":app")
include(":domain")
include(":repos")
include(":repos:base")
include(":data")
include(":data:database")
include(":data:network")
include(":data:network:base")
include(":data:network:movieapi")
include(":repos:movies")
include(":di")
